import { TComponent, TDynamicComponent } from "../utils/types";

interface IStateProps<IState extends {}> {
    state: Partial<IState>;
    setState: (changes: Partial<IState>) => void;
}

export const withState = <TState extends {}, TProps = {}>(
    Component: TComponent<TProps & IStateProps<TState>>, 
    initialState = {} as TState
): TDynamicComponent<TProps> => {
    const state = initialState;
    return (props: TProps) => (rerender) => {
        const setState = (changes) => {
            Object.assign(state, changes);
            rerender();
        };
        return Component({...props, state, setState});
    };
};

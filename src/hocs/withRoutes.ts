import { TComponent, TContent, TDynamicComponent } from "../utils/types";

type ROUTE = string; // TODO use project specific enum

export interface IRoutes<TProps = {}> {
    [key: string]: TComponent<TProps>;
}

export interface IURLParams {[key: string]: string}

const serializeUrlParams = (obj: IURLParams = {}) => {
    const str = [];
    for (const key in obj)
        if (obj.hasOwnProperty(key)) {
            str.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]));
        }
    return str.join("&");
};

const createUrl = (route: ROUTE, urlParams: IURLParams = {}) => {
    const serializedUrlParams = serializeUrlParams(urlParams);
    return location.origin + '/' +
        (serializedUrlParams ? '?' + serializeUrlParams(urlParams) : '') +
        '#' + route
};

export const getUrlParams = <T extends {}>(search: string): T => search
    .replace(/(^\?)/, '')
    .split("&")
    .reduce((obj, str) => {
        const [key, value] = str.split('=');
        obj[key] = value;
        return obj;
    }, {} as T);

export const getRoute = (hash: string) => hash.substr(1) as ROUTE;

export const navigate = (route: ROUTE, urlParams?: IURLParams) =>
    window.location.href = createUrl(route, urlParams);

export const withRoutes = <TProps = {}>(
    Component: TComponent, 
    routes: IRoutes<TProps>
): TDynamicComponent<TProps> => (props?: TProps) => {
    let currentContent: TContent;

    const updateContent = () => {
        const route = getRoute(location.hash);
        const RouteComponent = routes[route];
        currentContent = RouteComponent ? 
            RouteComponent(props) : 
            null;
    };

    updateContent();

    return rerender => {
        const changeRoute = () => {
            window.removeEventListener('hashchange', changeRoute);
            updateContent();
            rerender();
        };

        window.addEventListener('hashchange', changeRoute);

        return Component({}, currentContent);
    }
};

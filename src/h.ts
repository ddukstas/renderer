import { TElProps, TTagNames, TContent, INodeConfig, TComponent, TContentItem } from "./utils/types";
import { isContent } from "./utils/commonUtils";

type TBuilderMethod = (
    tag: TTagNames | TComponent,
    propsOrContent: TContent | TElProps, 
    ...content: Array<TContent>
) => TContentItem;

type TProxyMethod = (
    propsOrContent: TContent | TElProps, 
    ...content: Array<TContent>
) => INodeConfig;

type TBuilder = { [TagName in TTagNames]: TProxyMethod } & TBuilderMethod;


const buildConfig = (
    tag: TTagNames | TProxyMethod,
    propsOrContent: TContent | TElProps, 
    ...content: Array<TContent>
): TContentItem => {
    const arePropsOmitted = !propsOrContent || isContent(propsOrContent);

    const config: INodeConfig & {content: Array<TContent>} = { 
        content: [].concat(content).filter(c => !!c)
    };

    if (!arePropsOmitted) {
        const {
            onCreate, onRemove, onUpdate, ...props
        } = propsOrContent as TElProps;

        Object.assign(config, {props, onCreate, onRemove, onUpdate, content});
    } else if (propsOrContent) {
        config.content = [].concat(propsOrContent, config.content);
    }
    
    if (typeof(tag) === 'function') {
        return tag(!arePropsOmitted && propsOrContent, ...config.content);
    } else {
        config.tag = tag;
        return config;
    }
}

export const h = new Proxy<TBuilder>(buildConfig as any, {
    get(_, tag: TTagNames): TProxyMethod {
        return (props, content) => 
            buildConfig(tag, props, content) as INodeConfig;
    }
});

import { TContent, INodeConfig } from "./types";

export const getIsCreate = (newC: INodeConfig, oldC: INodeConfig) => 
    !!newC && !oldC;

export const getIsUpdate = (newC: INodeConfig, oldC: INodeConfig) => 
    !!newC && !!oldC && 
    newC !== oldC && 
    (!newC.shouldUpdate || newC.shouldUpdate(newC, oldC));

export const getIsRemove = (newC: INodeConfig, oldC: INodeConfig) =>  
    !newC && !!oldC;

// TODO remake with instanceof
export const isNodeConfig = obj => !!(obj && obj.content);

export const contentToList = (content: TContent) => 
    Array.isArray(content) ? content : [content];

export const contentToConfig = (content: TContent) => {
    const config = isNodeConfig(content) ? 
        {...content as INodeConfig}: 
        {content};

    config.content = contentToList(config.content);

    return config;
};

export const isContent = (obj: any) => !!obj && (
    typeof(obj) === 'string' || 
    isNodeConfig(obj) ||
    Array.isArray(obj)
);

export const isContentOnlyString = (content: TContent) => {
    content = contentToList(content);
    return content.length === 1 && typeof(content[0]) === 'string';
}

import { TElProps } from "./types";

const getIsEventName = (key: string) => key.substr(0, 2) === 'on';

const camelToKebabCase = (str: string) => 
    str.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();

const parseStyle = (style: {[s: string]: string} | string): string => {
    return typeof(style) === 'string' ? 
        style : 
        Object.keys(style).map(key => `${camelToKebabCase(key)}: ${style[key]}`).join('; ');
}

export const setProps = (node: Element, newProps: TElProps, oldProps?: TElProps) => {
    // do prop diff & update
    newProps && Object.keys(newProps).forEach(key => {
        const newValue = newProps[key];
        const oldValue = oldProps && oldProps[key];

        if (oldValue !== newValue) {
            if(key === 'style') {
                node.setAttribute(key, parseStyle(newValue))
            } else if(getIsEventName(key)) {
                const eventName = key.substr(2).toLowerCase();
                if (oldValue) {
                    node.removeEventListener(eventName, oldValue);
                }
                if (newValue) {
                    node.addEventListener(eventName, newValue)
                }
            } else {
                node.setAttribute(key, newValue);
            }
        }
    });

    // remove unset props
    oldProps && Object.keys(oldProps).forEach(key => {
        if (!newProps[key]) {
            if(getIsEventName(key)) {
                const eventName = key.substr(2).toLowerCase();
                node.removeEventListener(eventName, oldProps[key])
            } else {
                node.removeAttribute(key);
            }
        }
    });
}

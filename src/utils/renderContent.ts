import { setProps } from "./setProps";
import { TContent, INodeConfig, TContentItem, TContentItemGetter } from "./types";
import { contentToList, getIsCreate, getIsRemove, contentToConfig, getIsUpdate, isContentOnlyString } from "./commonUtils";

export const renderText = (
    node: Element, 
    text: string, 
    oldContent?: TContent
) => {
    if (text !== oldContent) {
        node.innerHTML = text || '';
    }
};

const KEY_ATTR = '_key';

const createChild = (tag?: string, key?: string) => {
    const el = document.createElement(tag || 'span');
    if (key) {
        el.setAttribute(KEY_ATTR, key);
    }
    return el;
}

type TConfigPair = [
    TContentItem,
    TContentItem,
    (parent: Element) => Element
]

const getConfigPairs = (
    newContent: TContent,
    oldContent?: TContent
): Array<TConfigPair> => {
    const newContentList = 
        contentToList(newContent) as Array<Partial<INodeConfig>>;
    const oldContentList = 
        contentToList(oldContent) as Array<Partial<INodeConfig>>;
        
    const newKeys = newContentList.map(c => !!c && c.key).filter(k => !!k);
    const uniqueOldKeys = oldContentList.map(c => !!c && c.key)
        .filter(k => !!k)
        .filter(k => !newKeys.includes(k));
    const keys = newKeys.concat(uniqueOldKeys);

    if (keys.length) {
        return keys.map(key => [
            newContentList.find(c => !!c && c.key === key),
            oldContentList.find(c => !!c && c.key === key),
            parent => parent.querySelector(`[${KEY_ATTR}='${key}']`)
        ]);
    } else {
        const largestIndex = Math.max(
            newContentList.length, 
            oldContentList.length
        ) - 1;
        
        let configPairs = [];
        for (let i = 0; i <= largestIndex; i++) {
            configPairs.push([
                newContentList[i], 
                oldContentList[i],
                parent => parent.children[i]
            ]);
        }
        return configPairs;
    }
}

export const renderChildren = (
    parent: Element, 
    newContent: TContent,
    oldContent?: TContent
) => {

    if (typeof(newContent) === 'string') {
        return renderText(parent, newContent, oldContent);
    }

    // assumption: no child nodes - no content
    if (isContentOnlyString(oldContent)) {
        parent.innerHTML = '';
        oldContent = [];
    }

    let childrenToRemove = [];
    getConfigPairs(newContent, oldContent).forEach(pair => {
        const [newConfigGetter, oldConfigGetter, getOldChild] = pair;
        let newConfig: Partial<INodeConfig>;
        let oldConfig: Partial<INodeConfig>;
        let child: Element;

        // used only after the first render
        const rerender = () => {
            oldConfig = newConfig;
            newConfig = (newConfigGetter as (r) => INodeConfig)(rerender);
            renderContent(child, newConfig, oldConfig);
        };

        newConfig = (typeof(newConfigGetter) === 'function' ? 
            newConfigGetter(rerender) :     
            newConfigGetter
        ) as INodeConfig;

        oldConfig = (typeof(oldConfigGetter) === 'function' ? 
            oldConfigGetter(rerender) :     
            oldConfigGetter
        ) as INodeConfig;

        const isCreate = getIsCreate(newConfig, oldConfig);
        const isRemove = getIsRemove(newConfig, oldConfig);
        const isUpdate = getIsUpdate(newConfig, oldConfig);

        child = isCreate ? 
            createChild(newConfig.tag || 'span', newConfig.key) :
            getOldChild(parent);
        
        if (isCreate || isUpdate) {
            renderContent(child, newConfig, oldConfig);
        }

        if (isCreate) {
            parent.appendChild(child);
            newConfig && newConfig.onCreate && newConfig.onCreate();
        } else if(isUpdate) {
            newConfig && newConfig.onUpdate && newConfig.onUpdate();
        } else if (isRemove) {
            childrenToRemove.push(child);
            oldConfig && oldConfig.onRemove && oldConfig.onRemove();    
        }
    });

    childrenToRemove.forEach(child => parent.removeChild(child));
}

/**
 * Recursively updates target node attributes & contents
 * according to configuration changes.
 */
export const renderContent = (
    node: Element, 
    content: TContent, 
    oldContent?: TContent
) => {
    const config = contentToConfig(content);
    const oldConfig = contentToConfig(oldContent);

    if (config.props) {
        setProps(node, config.props, oldConfig.props);
    }

    if (isContentOnlyString(config.content)) {
        renderText(
            node, 
            config.content[0], 
            oldConfig.content && oldConfig.content[0]
        );
    } else {
        renderChildren(node, config.content, oldConfig.content);
    }
}

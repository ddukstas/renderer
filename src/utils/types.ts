export type TElProps = {[s: string]: any};

export type TTagNames = keyof HTMLElementTagNameMap;

export type TContentItemGetter = (rerender: (() => void)) => 
    INodeConfig | string;

export type TContentItem = TContentItemGetter | INodeConfig | string;

export type TContent = Array<TContentItem> | TContentItem;

export type TComponent<TProps = {}> = (props?: TProps, children?: TContent) => 
    (INodeConfig | string);

export type TDynamicComponent<TProps = {}> = (props?: TProps, children?: TContent) => TContentItemGetter;

export interface INodeConfig {
    key?: string;
    tag?: TTagNames; // default: 'span'
    props?: TElProps;
    content?: TContent;
    onCreate?: () => void;
    onRemove?: () => void;
    onUpdate?: () => void;
    shouldUpdate?: (newConfig: INodeConfig, oldConfig: INodeConfig) => void;
}

import { setProps } from "../setProps";

describe('utils', () => {
    describe('setProps', () => {
        it('adds event listener', () => {
            const el = document.createElement('div');
            const onClick = jest.fn();
            setProps(el, {onClick});
            el.click();
            el.click();
            expect(onClick).toHaveBeenCalledTimes(2);
        });

        it('changes event listener', () => {
            const el = document.createElement('div');
            const onClick1 = jest.fn();
            const onClick2 = jest.fn();
            const initialProps = {onClick: onClick1};
            setProps(el, initialProps);
            setProps(el, {onClick: onClick2}, initialProps);
            el.click();
            expect(onClick1).toHaveBeenCalledTimes(0);
            expect(onClick2).toHaveBeenCalledTimes(1);
        });

        it('removes event listener', () => {
            const el = document.createElement('div');
            const onClick = jest.fn();
            const initialProps = {onClick};
            setProps(el, initialProps);
            setProps(el, {}, initialProps);
            el.click();
            expect(onClick).toHaveBeenCalledTimes(0);
        });

        it ('parses & sets style object', () => {
            const el = document.createElement('div');
            setProps(el, {style: {color: 'red', backgroundColor: 'blue'}});
            expect(el.getAttribute('style')).toBe('color: red; background-color: blue');
        });
    });
});

import { renderText, renderChildren, renderContent } from "../renderContent";

describe('utils', () => {
    describe('renderText', () => {
        it('sets text to an element', () => {
            const el = document.createElement('div');
            renderText(el, 'asdf');
            expect(el.innerHTML).toBe('asdf');
        });

        it('removes existing text', () => {
            const el = document.createElement('div');
            const initialText = 'asdf';
            renderText(el, initialText);
            renderText(el, '', initialText);
            expect(el.innerHTML).toBe('');
        });
    });

    describe('renderChildren', () => {
        it('sets children to an element', () => {
            const el = document.createElement('div');
            renderChildren(el, [
                {content: 'ciuvas1'},
                {content: 'bicas2'}
            ]);

            expect(el.innerHTML).toContain('ciuvas1');
            expect(el.innerHTML).toContain('bicas2');
        });

        it('removes child from an element', () => {
            const el = document.createElement('div');
            const initialConfig = [
                {content: 'ciuvas1'},
                {content: 'bicas2'}
            ];
            renderChildren(el, initialConfig);
            renderChildren(el, [{content: 'bicas2'}], initialConfig);
            expect(el.innerHTML).toContain('bicas2');
        });

        it('does not rerender unchanged siblings when keys are set', () => {
            const el = document.createElement('div');
            const initialConfig = [
                {key: 'c', content: 'ciuvas1'},
                {key: 'b', content: 'bicas2'}
            ];
            renderChildren(el, initialConfig);
            const bicasEl = el.children[1];

            renderChildren(el, [{key: 'b', content: 'bicas2'}], initialConfig);
            expect(el.children).toHaveLength(1);
            expect(el.children[0]).toBe(bicasEl);
        });

        it('overwrites a child', () => {
            const el = document.createElement('div');
            const initialConfig = [
                {content: 'ciuvas1'},
                {content: 'bicas2'}
            ];

            renderChildren(el, initialConfig);
            renderChildren(el, [
                {content: 'ciuvas3'},
                {content: 'bicas2'}
            ], initialConfig);

            expect(el.children).toHaveLength(2);
            expect(el.innerHTML).toContain('ciuvas3');
            expect(el.innerHTML).toContain('bicas2');
            expect(el.innerHTML).not.toContain('ciuvas1');
        });
    });

    describe('hooks', () => {
        it('calls onCreate one time (on node creation)', () => {
            const el = document.createElement('div');
            const onCreate = jest.fn();
            const initialConfig = [
                {key: 'c', content: 'ciuvas1', onCreate},
                {key: 'b', content: 'bicas2'}
            ];

            renderChildren(el, initialConfig);
            expect(onCreate).toHaveBeenCalledTimes(1);

            renderChildren(el, [
                {key: 'c', content: 'ciuvas1', onCreate}
            ], initialConfig);
            expect(onCreate).toHaveBeenCalledTimes(1);
        });

        it('calls onCreate when key changes', () => {
            const el = document.createElement('div');
            const onCreate = jest.fn();
            const initialConfig = [{key: 'c', content: 'ciuvas1', onCreate}];

            renderChildren(el, initialConfig);
            expect(onCreate).toHaveBeenCalledTimes(1);

            renderChildren(el, [
                {key: 'd', content: 'ciuvas2', onCreate}
            ], initialConfig);
            expect(onCreate).toHaveBeenCalledTimes(2);
        });

        it('calls onCreate one time for a single unchanged node without a key', () => {
            const el = document.createElement('div');
            const onCreate = jest.fn();
            const initialConfig = [{content: 'ciuvas1', onCreate}];

            renderChildren(el, initialConfig);
            expect(onCreate).toHaveBeenCalledTimes(1);

            renderChildren(el, [{content: 'ciuvas2', onCreate}], initialConfig);
            expect(onCreate).toHaveBeenCalledTimes(1);
        });

        it('calls onRemove one time (on node removal)', () => {
            const el = document.createElement('div');
            const onRemove = jest.fn();
            const initialConfig = [
                {key: 'c', content: 'ciuvas1'},
                {key: 'b', content: 'bicas2', onRemove}
            ];

            renderChildren(el, initialConfig);
            expect(onRemove).toHaveBeenCalledTimes(0);

            renderChildren(el, [{key: 'c', content: 'ciuvas1'}], initialConfig);
            expect(onRemove).toHaveBeenCalledTimes(1);
        });

        it('does not call onRemove for remaining node', () => {
            const el = document.createElement('div');
            const onRemove = jest.fn();
            const initialConfig = [
                {key: 'c', content: 'ciuvas1', onRemove},
                {key: 'b', content: 'bicas2'}
            ];

            renderChildren(el, initialConfig);
            expect(onRemove).toHaveBeenCalledTimes(0);

            renderChildren(el, [{key: 'c', content: 'ciuvas1'}], initialConfig);
            expect(onRemove).toHaveBeenCalledTimes(0);
        });

        it('does not call onUpdate on node creation', () => {
            const el = document.createElement('div');
            const onUpdate = jest.fn();
            const initialConfig = [{key: 'c', content: 'ciuvas1', onUpdate}];

            renderChildren(el, initialConfig);
            expect(onUpdate).toHaveBeenCalledTimes(0);
        });

        it('calls onUpdate when node changes', () => {
            const el = document.createElement('div');
            const onUpdate = jest.fn();
            const initialConfig = [
                {key: 'c', content: 'ciuvas1', onUpdate},
                {key: 'b', content: 'bicas2'}
            ];

            renderChildren(el, initialConfig);
            expect(onUpdate).toHaveBeenCalledTimes(0);

            const updatedConfig = [{key: 'c', content: 'ciuvas1', onUpdate}];

            renderChildren(el, updatedConfig, initialConfig);
            renderChildren(el, updatedConfig, updatedConfig);
            expect(onUpdate).toHaveBeenCalledTimes(1);
        });

        it('does not rerender when shouldUpdate returns false', () => {
            const el = document.createElement('div');
            const initialConfig = [{
                content: [{content: 'ciuvas1'}], 
                shouldUpdate: () => false
            }];
            renderChildren(el, initialConfig);

            const newConfig = {
                content: [{content: 'bicas2'}], 
                shouldUpdate: () => false
            };
            renderChildren(el, newConfig, initialConfig);
            expect(el.innerHTML).toContain('ciuvas1');
            expect(el.innerHTML).not.toContain('bicas2');
        });
    });

    describe('state', () => {
        it('takes function as config getter', () => {
            const el = document.createElement('div');
            const initialConfig = {content: [
                () => ({content: 'ciuvas1'}),
                () => ({content: 'bicas2'})
            ]};
            renderContent(el, initialConfig);
            expect(el.innerHTML).toContain('ciuvas1');
            expect(el.innerHTML).toContain('bicas2');
        });
    });

    describe('renderContent', () => {
        it('changes child nodes to text', () => {
            const el = document.createElement('div');
            const initialConfig = {content: [
                {content: 'ciuvas1'},
                {content: 'bicas2'}
            ]};
            renderContent(el, initialConfig);
            renderContent(el, {content: 'asdf'}, initialConfig);
            expect(el.innerHTML).toBe('asdf');
        });

        it('changes text to child nodes', () => {
            const el = document.createElement('div');
            const initialConfig = {content: 'asdf'};
            renderContent(el, initialConfig);
            renderContent(el, {content: [{content: 'ciuvas1'}]}, initialConfig);
            
            expect(el.children).toHaveLength(1);
            expect(el.innerHTML).toContain('ciuvas1');
        });

        it('does not re-render when contents remain the same', () => {
            const el = document.createElement('div');
            const config = {content: [
                {content: 'ciuvas1'},
                {content: 'bicas2'}
            ]};
            renderContent(el, config);
            const child1 = el.children[0];
            const child2 = el.children[1];
            renderContent(el, config);

            expect(child1).toEqual(el.children[0]);
            expect(child2).toEqual(el.children[1]);
        });
    });

});

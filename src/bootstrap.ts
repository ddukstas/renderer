import { TComponent, TDynamicComponent } from "./utils/types";
import { renderContent } from "./utils/renderContent";

export const bootstrap = (
    root: Element, 
    Component: TComponent | TDynamicComponent, 
    shouldAppend = true
) => {
    if (shouldAppend) {
        const newRoot = document.createElement('div');
        root.appendChild(newRoot);
        root = newRoot;
    }
    const render = () => renderContent(root, Component());
    render();
    return render;
}

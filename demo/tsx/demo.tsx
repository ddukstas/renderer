import {bootstrap, h, withState, withRoutes} from '../../src/index';

interface ICounterState {
    nb: number;
}

interface ICounterProps {
    initialNb: number;
}

const Counter = withState<ICounterState, ICounterProps>(props => (
    <div
        onCreate={() => props.setState({ nb: props.initialNb })}
        onClick={() => props.setState({ nb: props.state.nb + 1 })}
    >
        <div>::::::</div>
        <button>{'#' + props.state.nb}</button>
    </div>
));


const Home = () => (
    <div>
        <span>{':3'}</span>
        <span>:ę</span>
        <div key="b" >:D</div>
        <div key="c" >:#</div>
        <Counter initialNb={2} />
    </div>
);

const Root = withRoutes(h.div, {
    '': Home,
    'home': Home,
    'about': () => h.div('bout bout bout about')
})


bootstrap(document.body, Root);

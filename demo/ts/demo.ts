import {bootstrap, h, withState} from '../../src/index';

interface ICounterState {
    nb: number;
}

interface ICounterProps {
    initialNb: number;
}

const Counter = withState<ICounterState, ICounterProps>(props => h.div({
    onCreate: () => props.setState({ nb: props.initialNb }),
    onClick: () => props.setState({ nb: props.state.nb + 1 })
}, [
    h.div('::::::'),
    h.button('#' + props.state.nb)
]));

const Root = () => h.div([
    ':3',
    ':)',
    h.div({key: 'b'}, ':D'),
    h.div({key: 'c'}, ':#'),
    Counter({initialNb: 1})
]);

bootstrap(document.body, Root);
